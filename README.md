# rickandmorty

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

#### Pruebas flutter install
```shell
$ flutter
$ flutter --version
$ flutter sdk-path
$ flutter doctor
```

#### UpGrade SDK flutter

$ flutter upgrade
$ flutter --version
$ flutter sdk-path
$ flutter doctor
```

### Instalación de Android Studio
El procedimiento está detallado en [https://developer.android.com/studio/install#linux](https://developer.android.com/studio/install#linux).

#### Ejecución del programa
```shell
$ flutter clean
$ flutter pub get
$ flutter run 
```

#### Versiones necesarias
```
Android Studio (version 4.1)
Flutter (Channel dev, 2.10.0-0.3.pre, on macOS 12.0.1 21A559 darwin-arm, locale es-CO)
Xcode - develop for iOS and macOS (Xcode 13.2.1)
Android toolchain - develop for Android devices (Android SDK version 30.0.3)
```