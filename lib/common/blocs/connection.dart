import 'package:connectivity/connectivity.dart';

class Connection {
  Future<bool> isConnected() async {
    final bool data;
    var connectivityResult = await (Connectivity().checkConnectivity());
    switch (connectivityResult) {
      case ConnectivityResult.none:
        data = false;
        break;
      case ConnectivityResult.mobile:
        data = true;
        break;
      case ConnectivityResult.wifi:
        data = true;
        break;
      default:
        data = false;
    }
    return data;
  }
}

final connection = Connection();
