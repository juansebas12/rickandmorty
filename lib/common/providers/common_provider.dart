import 'dart:convert';
import 'package:http/http.dart' as Http;
import 'package:http/http.dart';

class CommonProvider {
  nextInList(String url) async {
    Response httpResponse = await Http.get(Uri.parse(url));
    final response = json.decode(utf8.decode(httpResponse.bodyBytes));
    return response;
  }
}
