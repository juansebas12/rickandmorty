class LocationModel {
  late List<Location> results;
  LocationModel({required this.results});
  LocationModel.fromJson(json) {
    results = <Location>[];
    json.forEach((v) {
      results.add(new Location.fromJson(v));
    });
  }
}

class Location {
  Location({
    required this.name,
    required this.url,
  });

  String name;
  String url;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        name: json["name"],
        url: json["url"],
      );
}
