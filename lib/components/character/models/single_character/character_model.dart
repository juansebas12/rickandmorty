import 'package:rickandmorty/components/character/models/single_character/location_model.dart';

class CharacterSingleModel {
  late Character results;
  CharacterSingleModel({required this.results});
  CharacterSingleModel.fromJson(json) {
    if (json != null) {
      results = new Character.fromJson(json);
    }
  }
}

class CharacterModel {
  late List<Character> results;
  CharacterModel({required this.results});
  CharacterModel.fromJson(json) {
    results = <Character>[];
    json.forEach((v) {
      results.add(new Character.fromJson(v));
    });
  }
}

class Character {
  int id;
  String name;
  String status;
  String species;
  String type;
  String gender;
  Location origin;
  Location location;
  String image;
  List<String> episode;
  String url;
  DateTime created;
  Character({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.image,
    required this.episode,
    required this.url,
    required this.created,
  });

  factory Character.fromJson(Map<String, dynamic> json) => Character(
        id: json["id"],
        name: json["name"],
        status: json["status"],
        species: json["species"],
        type: json["type"],
        gender: json["gender"],
        origin: Location.fromJson(json["origin"]),
        location: Location.fromJson(json["location"]),
        image: json["image"],
        episode: List<String>.from(json["episode"].map((x) => x)),
        url: json["url"],
        created: DateTime.parse(json["created"]),
      );
}
