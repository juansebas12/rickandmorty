import 'package:rickandmorty/components/character/models/response_models/info_model.dart';
import 'package:rickandmorty/components/character/models/single_character/character_model.dart';

class CharacterResponseModel {
  late CharacterResponse results;
  CharacterResponseModel({required this.results});
  CharacterResponseModel.fromJson(json) {
    if (json != null) {
      results = new CharacterResponse.fromJson(json);
    }
  }
}

class CharacterResponse {
  CharacterResponse({
    required this.info,
    required this.results,
  });

  Info info;
  List<Character> results;

  factory CharacterResponse.fromJson(Map<String, dynamic> json) =>
      CharacterResponse(
        info: Info.fromJson(json["info"]),
        results: CharacterModel.fromJson(json['results']).results,
      );
}
