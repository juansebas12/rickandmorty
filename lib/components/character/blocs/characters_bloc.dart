import 'dart:developer';

import 'package:rickandmorty/common/blocs/connection.dart';
import 'package:rickandmorty/common/providers/common_provider.dart';
import 'package:rickandmorty/components/character/models/response_models/characters_response_model.dart';
import 'package:rickandmorty/components/character/models/single_character/character_model.dart';
import 'package:rickandmorty/components/character/providers/character_provider.dart';
import 'package:rxdart/rxdart.dart';

class CharactersBloc {
  late CharacterResponse characterResponse;
  // ignore: close_sinks
  BehaviorSubject<CharacterResponse> charactersController =
      BehaviorSubject<CharacterResponse>();
  Stream<CharacterResponse> get charactersStream => charactersController.stream;

  bool loading = false;
  BehaviorSubject<bool> _loadingController = BehaviorSubject<bool>();

  final provider = CharacterProvider();
  final commonProvider = CommonProvider();

  loadCharacters() async {
    if (await connection.isConnected()) {
      var response = await provider.LoadAllCharater();
      characterResponse = CharacterResponseModel.fromJson(response).results;
      charactersController.sink.add(characterResponse);
    }
  }

  loadingNextCharacteres() async {
    try {
      if (characterResponse.info.next != null && !loading) {
        loading = true;
        _loadingController.sink.add(true);
        var response =
            await commonProvider.nextInList(characterResponse.info.next);
        characterResponse.info.next = response['info']['next'];
        characterResponse.info.prev = response['info']['prev'];
        characterResponse.results
            .addAll(CharacterModel.fromJson(response['results']).results);
        charactersController.sink.add(characterResponse);
        _loadingController.sink.add(false);
        loading = false;
      }
    } catch (e) {
      _loadingController.sink.add(false);
      loading = false;
    }
  }
}

final charactersBloc = CharactersBloc();
