import 'package:rickandmorty/common/blocs/connection.dart';
import 'package:rickandmorty/components/character/models/single_character/character_model.dart';
import 'package:rickandmorty/components/character/providers/character_provider.dart';
import 'package:rxdart/rxdart.dart';

class CharacterBloc {
  late Character characterResponse;
  // ignore: close_sinks
  BehaviorSubject<Character> characterController = BehaviorSubject<Character>();
  Stream<Character> get characterStream => characterController.stream;

  final provider = CharacterProvider();

  loadCharacter(String idCharacter) async {
    if (await connection.isConnected()) {
      var response = await provider.LoadAllCharater();
      characterResponse = CharacterSingleModel.fromJson(response).results;
      characterController.sink.add(characterResponse);
    }
  }
}

final characterBloc = CharacterBloc();
