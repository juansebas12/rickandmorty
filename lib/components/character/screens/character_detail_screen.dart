import 'package:flutter/material.dart';
import 'package:rickandmorty/components/character/models/single_character/character_model.dart';

class CharacterInfoScreen extends StatelessWidget {
  final Character character;
  const CharacterInfoScreen({Key? key, required this.character})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(character.name),
        ),
        body: Center(
            child: Wrap(
          children: [
            Container(
                width: 400,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 5),
                    borderRadius: BorderRadius.circular(20)),
                child: _infoCharacter()),
          ],
        )));
  }

  Widget _infoCharacter() {
    return Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        elevation: 10,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 12, top: 12),
              child: const Center(
                child: Text(
                  'Detalles',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image.network(character.image),
              ),
            ),
            Center(child: _textDetails(character.name)),
            _textDetails('gender : ${character.gender}'),
            _textDetails('origin : ${character.origin.name}'),
            _textDetails('location : ${character.location.name}'),
            _textDetails('number episoder : ${character.episode.length}'),
          ],
        ));
  }

  Widget _textDetails(String text) {
    return Container(
      margin: EdgeInsets.only(bottom: 12, top: 12, left: 16),
      child: Text(
        text,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    );
  }
}
