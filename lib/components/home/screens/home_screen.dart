import 'package:flutter/material.dart';
import 'package:rickandmorty/common/blocs/connection.dart';
import 'package:rickandmorty/components/character/blocs/characters_bloc.dart';
import 'package:rickandmorty/components/character/models/response_models/characters_response_model.dart';
import 'package:rickandmorty/components/character/models/single_character/character_model.dart';
import 'package:rickandmorty/components/episode/blocs/episode_bloc.dart';
import 'package:rickandmorty/components/home/widgets/card_info_character.dart';
import 'package:rickandmorty/components/home/widgets/header_data_serie.dart';
import 'package:rickandmorty/components/location/blocs/location_bloc.dart';

class HomeScreen extends StatelessWidget {
  final _listController = ScrollController();
  HomeScreen({Key? key}) {
    _listController.addListener(() {
      var maxScroll = _listController.position.maxScrollExtent;
      var position = _listController.position.pixels;
      if (position > maxScroll * 0.5) {
        charactersBloc.loadingNextCharacteres();
      }
    });
    locationBloc.loadLocation();
    charactersBloc.loadCharacters();
    episodeBloc.loadEpisodes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Rick and Morty'),
        ),
        body: FutureBuilder<bool>(
            future: connection.isConnected(),
            initialData: false,
            builder: ((context, snapshot) {
              return homeLoading(snapshot.data);
            })));
  }

  Widget homeLoading(bool? data) {
    if (data != null && data) {
      return Column(
        children: [HeaderDataSerie(), Expanded(child: listCharacters())],
      );
    } else {
      return Container(
        child: Center(
          child: Container(
            margin: EdgeInsets.only(bottom: 12, top: 12),
            child: const Center(
              child: Text(
                'NO HAY CONEXION A INTERNET',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      );
    }
  }

  Widget listCharacters() {
    return StreamBuilder<CharacterResponse>(
        stream: charactersBloc.charactersStream,
        builder: (context, snapshot) {
          return !snapshot.hasData
              ? Container()
              : ListView.builder(
                  controller: _listController,
                  itemCount: snapshot.data?.results.length,
                  itemBuilder: (BuildContext context, int index) {
                    Character character = snapshot.data!.results[index];
                    return CardInfoCharacter(character: character);
                  });
        });
  }
}
