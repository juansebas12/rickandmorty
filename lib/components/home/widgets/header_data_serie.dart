import 'package:flutter/material.dart';
import 'package:rickandmorty/components/episode/blocs/episode_bloc.dart';
import 'package:rickandmorty/components/episode/models/episodes_results.dart';
import 'package:rickandmorty/components/location/blocs/location_bloc.dart';

class HeaderDataSerie extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.grey,
      padding: const EdgeInsets.all(16.0),
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          const Text('la serie en números',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold)),
          StreamBuilder<EpisodeResponse>(
              stream: episodeBloc.episodeResponseStream,
              builder: (context, snapshot) {
                String numberEpisodes = snapshot.hasData
                    ? 'número de episodios : ${snapshot.data!.info.count}'
                    : "loading ....";
                return Text(numberEpisodes,
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold));
              }),
          StreamBuilder<String>(
              stream: locationBloc.locationCountStream,
              builder: (context, snapshot) {
                String locationMax = snapshot.hasData
                    ? 'location con más personajes: ${snapshot.data}'
                    : "loading ....";
                return Text(locationMax,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold));
              }),
        ],
      ),
    );
  }
}
