import 'package:rickandmorty/common/blocs/connection.dart';
import 'package:rickandmorty/components/episode/models/episodes_results.dart';
import 'package:rickandmorty/components/episode/provider/episode_provider.dart';
import 'package:rxdart/rxdart.dart';

class EpisodeBloc {
  late EpisodeResponse episodeResponse;
  // ignore: close_sinks
  BehaviorSubject<EpisodeResponse> episodeResponseController =
      BehaviorSubject<EpisodeResponse>();
  Stream<EpisodeResponse> get episodeResponseStream =>
      episodeResponseController.stream;

  final provider = EpisodeProvider();

  loadEpisodes() async {
    if (await connection.isConnected()) {
      var response = await provider.LoadEpisode();
      episodeResponse = EpisodeResponseModel.fromJson(response).results;
      episodeResponseController.sink.add(episodeResponse);
    }
  }
}

final episodeBloc = EpisodeBloc();
