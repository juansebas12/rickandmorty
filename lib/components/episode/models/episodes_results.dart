import 'package:rickandmorty/components/character/models/response_models/info_model.dart';
import 'package:rickandmorty/components/character/models/single_character/character_model.dart';

class EpisodeResponseModel {
  late EpisodeResponse results;
  EpisodeResponseModel({required this.results});
  EpisodeResponseModel.fromJson(json) {
    if (json != null) {
      results = new EpisodeResponse.fromJson(json);
    }
  }
}

class EpisodeResponse {
  EpisodeResponse({
    required this.info,
  });

  Info info;

  factory EpisodeResponse.fromJson(Map<String, dynamic> json) =>
      EpisodeResponse(
        info: Info.fromJson(json["info"]),
      );
}
