import 'package:rickandmorty/common/blocs/connection.dart';
import 'package:rickandmorty/common/providers/common_provider.dart';
import 'package:rickandmorty/components/location/models/location_model.dart';
import 'package:rickandmorty/components/location/models/location_results_model.dart';
import 'package:rickandmorty/components/location/provider/location_provider.dart';
import 'package:rxdart/rxdart.dart';

class LocationBloc {
  late LocationResponse locationResponse;
  // ignore: close_sinks
  BehaviorSubject<LocationResponse> locationsController =
      BehaviorSubject<LocationResponse>();
  Stream<LocationResponse> get locationsStream => locationsController.stream;

  bool loading = false;
  BehaviorSubject<bool> _loadingController = BehaviorSubject<bool>();

  String location = "";
  BehaviorSubject<String> _locationConuntController = BehaviorSubject<String>();
  Stream<String> get locationCountStream => _locationConuntController.stream;

  final provider = LocationProvider();
  final commonProvider = CommonProvider();

  loadLocation() async {
    if (await connection.isConnected()) {
      var response = await provider.LoadLocation();
      locationResponse = LocationResponseModel.fromJson(response).results;
      locationsController.sink.add(locationResponse);
      await loadingNextLocations();
      int count = 0;
      for (var i = 0; i < locationResponse.results.length; i++) {
        if (locationResponse.results[i].episode.length > count) {
          count = locationResponse.results[i].episode.length;
          location = locationResponse.results[i].name;
        }
      }
      _locationConuntController.sink.add(location);
    }
  }

  loadingNextLocations() async {
    try {
      if (locationResponse.info.next != null && !loading) {
        loading = true;
        _loadingController.sink.add(true);
        var response =
            await commonProvider.nextInList(locationResponse.info.next);
        locationResponse.info.next = response['info']['next'];
        locationResponse.info.prev = response['info']['prev'];
        locationResponse.results
            .addAll(LocationModel.fromJson(response['results']).results);
        locationsController.sink.add(locationResponse);
        _loadingController.sink.add(false);
        loading = false;
        await loadingNextLocations();
      }
    } catch (e) {
      _loadingController.sink.add(false);
      loading = false;
    }
  }
}

final locationBloc = LocationBloc();
