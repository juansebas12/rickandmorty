class LocationModel {
  late List<Location> results;
  LocationModel({required this.results});
  LocationModel.fromJson(json) {
    results = <Location>[];
    json.forEach((v) {
      results.add(new Location.fromJson(v));
    });
  }
}

class Location {
  int id;
  String name;
  List<String> episode;
  Location({
    required this.id,
    required this.name,
    required this.episode,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        name: json["name"],
        episode: List<String>.from(json["residents"].map((x) => x)),
      );
}
