import 'package:rickandmorty/components/character/models/response_models/info_model.dart';
import 'package:rickandmorty/components/location/models/location_model.dart';

class LocationResponseModel {
  late LocationResponse results;
  LocationResponseModel({required this.results});
  LocationResponseModel.fromJson(json) {
    if (json != null) {
      results = new LocationResponse.fromJson(json);
    }
  }
}

class LocationResponse {
  LocationResponse({required this.info, required this.results});

  Info info;
  List<Location> results;

  factory LocationResponse.fromJson(Map<String, dynamic> json) =>
      LocationResponse(
        info: Info.fromJson(json["info"]),
        results: LocationModel.fromJson(json['results']).results,
      );
}
