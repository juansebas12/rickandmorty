import 'dart:convert';
import 'package:http/http.dart' as Http;
import 'package:http/http.dart';

class LocationProvider {
  LoadLocation() async {
    try {
      Response httpResponse =
          await Http.get(Uri.parse('https://rickandmortyapi.com/api/location'));
      final response = json.decode(utf8.decode(httpResponse.bodyBytes));
      return response;
    } catch (e) {
      return null;
    }
  }
}
